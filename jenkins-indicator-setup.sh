#!/bin/bash

# Store all of the files in ~/Repos directory (create if it doesn't exist)
if [ ! -d "~/Repos" ]; then
  mkdir ~/Repos
fi
cd ~/Repos

# Increase swap size to 256MB due to possible exhaust of virtual memory
# on RPi A/B Rev1 during RF24 library compilation.
sed -i "s/\(CONF_SWAPSIZE*=*\).*/\1256/" /etc/dphys-swapfile

# Install all necessary packages
apt-get update
apt-get -y upgrade all
apt-get -y install git libboost1.50-all python-jenkinsapi python-setuptools python-enum34 gcc-avr binutils-avr avr-libc gdb-avr avrdude espeak

# Clone, build and install RF24 library
git clone https://github.com/TMRh20/RF24.git
cd ~/Repos/RF24
make install

# Build and install RF24's python wrapper
cd ~/Repos/RF24/pyRF24
./setup.py build
./setup.py install

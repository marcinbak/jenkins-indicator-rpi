#!/usr/bin/python

from subprocess import call
from os.path import exists
from os.path import dirname
from os import makedirs
import sys


ROOT_DIR = dirname(__file__)
GEN_DIR = ROOT_DIR + "/gen"

BINARY_FILE ='gen/config.bin'
HEX_FILE ='gen/config.hex'


FLASH_DEV = ('/dev/ttyUSB0','/dev/tty.wchusbserial1410','/dev/tty.wchusbserial1420','/dev/tty.usbserial-A9SBJHLP')

if len(sys.argv) == 2 :
    FLASH_DEV = (sys.argv[1])
    print "Using device "+sys.argv[1]


if not exists(GEN_DIR):
    makedirs(GEN_DIR)

print "Enter device code (1-255). Device code should be unique, unless you want several devices react on the same message."
device_code = chr(input())
print "Device code {}".format(ord(device_code))
print "Enter number of pixels on device (1-255). Typical values 8 or 16"
pixel_num = chr(input())
print "Pixel number {}".format(ord(pixel_num))

with open(BINARY_FILE, 'w') as f:
    f.write(device_code)
    f.write(pixel_num)

call(['avr-objcopy', '-Ibinary', '-Oihex', BINARY_FILE, HEX_FILE])

dev = None
for d in FLASH_DEV:

    if exists(d):
        dev = d
        break

if dev is None:
    print "failed to find flashing device"
    print FLASH_DEV
    exit(-1)

print "Selected device {}".format(d)
#avrdude avrdude  -patmega328p -carduino -P/dev/ttyUSB0 -b57600 -D -Ueeprom:w:{}:i
call(['avrdude','-patmega328p','-carduino','-P'+dev,'-b57600', '-D', '-Ueeprom:w:{}:i'.format(HEX_FILE)])

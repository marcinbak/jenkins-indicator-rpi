import datetime

import pytz
import subprocess

from nrf24_wrapper import NRF24Wrapper, IndicatorStatus
import os
import logging

log = logging.getLogger('jenkins_indicator')


class Observer(object):
    def on_new_build_info(self, name, description, is_success, is_failed, was_good_previously, is_running, start_time,
                          estimated_time):
        pass

    def on_no_info_available(self, name):
        pass


class RNFObserver(Observer):
    def __init__(self, configuration):
        self.rf = NRF24Wrapper(0x65646f4e32)
        self.configuration = configuration

    def on_new_build_info(self, name, description, is_success, is_failed, was_good_previously, is_running, start_time,
                          estimated_time):

        status = [IndicatorStatus.Off, 0.5]
        if name in self.configuration.keys():

            if is_running:
                now = pytz.utc.localize(datetime.datetime.utcnow())
                status[1] = (now - start_time).total_seconds() / estimated_time.total_seconds()
                if was_good_previously:
                    status[0] = IndicatorStatus.InProgressLastSucceeded
                else:
                    status[0] = IndicatorStatus.InProgressLastFailed
            else:
                status[1] = 1.0
                if is_success:
                    status[0] = IndicatorStatus.Succeeded
                if is_failed:
                    status[0] = IndicatorStatus.Failed
            log.debug("Sending information for {} : {}".format(name, ','.join(map(str, status))))
            self.rf.send_project_info(self.configuration[name], *status)

    def on_no_info_available(self, name):
        if name in self.configuration.keys():
            self.rf.send_project_info(self.configuration[name], IndicatorStatus.Off, 0)


class AudioFailureObserver(Observer):
    def __init__(self, configuration, audio_file):
        self.is_oks = {}
        self.configuration = configuration
        self.audio_file = audio_file
        for c in configuration.keys():
            self.is_oks[c] = True

    def on_no_info_available(self, name):
        self.is_oks[name] = IndicatorStatus.Off

    def on_new_build_info(self, name, description, is_success, is_failed, was_good_previously, is_running, start_time,
                          estimated_time):
        if not is_running:
            previous_good = self.is_oks[name]

            if is_success:
                self.is_oks[name] = True

            if is_failed:
                self.is_oks[name] = False

            now_ok = self.is_oks[name]

            if (previous_good and (not now_ok)):
                subprocess.call(("omxplayer", self.audio_file))


class TTSFailureObserver(Observer):
    def __init__(self, configuration):
        self.is_oks = {}
        self.configuration = configuration
        for c in configuration.keys():
            self.is_oks[c] = True

    def on_no_info_available(self, name):
        self.is_oks[name] = IndicatorStatus.Off

    def on_new_build_info(self, name, description, is_success, is_failed, was_good_previously, is_running, start_time,
                          estimated_time):
        template_fail = "espeak  -ven-german --stdout '{}' | aplay"
        template_success = "espeak -s140 -ven-german-5+f6 --stdout '{}' | aplay"
        if not is_running:
            previous_good = self.is_oks[name]

            if is_success:
                self.is_oks[name] = True

            if is_failed:
                self.is_oks[name] = False

            now_ok = self.is_oks[name]

            if previous_good and (not now_ok):
                message = '{} - Build has been broken'.format(description)
                log.debug("Speaking message :{}".format(message))
                command = template_fail.format(message)
                log.debug("running command =" + command)
                os.system(command)
            if (not previous_good) and now_ok:
                message = '{} - Build is back to normal'.format(description)
                log.debug("Speaking message :{}".format(message))
                command = template_success.format(message)
                log.debug("running command =" + command)
                os.system(command)

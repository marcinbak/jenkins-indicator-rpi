#!/usr/bin/python

from unittest import TestCase, main
from jenkins_observers import TTSFailureObserver, AudioFailureObserver


class TestTTSFailureObserver(TestCase):
    def test_on_new_build_info(self):
        observer = TTSFailureObserver({"SO1-android": 14})
        observer.on_new_build_info("SO1-android", "SO one project", False, True, False, 0, 0)
        observer.on_new_build_info("SO1-android", "SO one project", True, False, False, 0, 0)


if __name__ == '__main__':
    main()

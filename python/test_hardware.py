#!/usr/bin/python
from RF24 import *
import RPi.GPIO as GPIO
import time
import sys


devices = []
states = [1, 2, 3, 4]
if (len(sys.argv) == 1):
    print "Please provide devices id to ping"
    exit(1)
else:
    devices = map(int,sys.argv[1:])

print "pinging devices:"
print devices

broadcast_address = 0x65646f4e32

radio = RF24(25, 0)

radio.begin()
radio.setRetries(0, 0)
radio.setChannel(70)
radio.setAutoAck(False)
radio.setPALevel(RF24_PA_MAX)
radio.setDataRate(RF24_250KBPS)
radio.openWritingPipe(broadcast_address)
radio.printDetails()

radio.stopListening()

devices = []
states = [1, 2, 3, 4]
if (len(sys.argv) == 1):
    print "Please provide devices id to ping"
    exit(1)
else:
    devices = map(int,sys.argv[1:])

progresses = [64, 96, 128, 160, 208]

while (True):
    for state in states:
        for progress in progresses:
            for device in devices:
                message = chr(device) + chr(state) + chr(progress)
                print "state {} {}".format(state, progress)
                radio.write(message)
		time.sleep(2)

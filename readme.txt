# README #

This repository is a part of CI Indicators project, described in detail on Neofonie Tech Blog.

### What is this repository for? ###

* Raspberry Pi part of the project CI Indicators
* Version 1.0

### How do I get set up? ###

You can launch our configuration script jenkins-indicator-setup.sh (with superuser proviledges) or perform the following steps manually:

1. Checkout the project git repository from Bitbucket https://bitbucket.org/neofoniemobile/jenkins-indicator-rpi.git master.

2. Install the following packages on Raspbian:

* git

* libboost1.50-all

* python-jenkinsapi

* python-setuptools

* python-enum34

* gcc-avr

* binutils-avr avr-libc

* gdb-avr avrdude espeak

For Raspberry Pi 1 or 0, extend swap size twice.

Download, compile, and install the RF24 library (can be found here: https://github.com/TMRh20/RF24).


# Setting up Raspberry Pi service

After successful tests, you can proceed with the configuring service on Raspberry Pi. In the `sample_service` directory you can find template files that should be placed in `/etc/indicator` and `/etc/init.d`.

Please proceed with the following steps:

1. Set your Jenkins credentials in server_config.json file (password corresponds to the Jenkins API access token which can be generated under: `http://[your.jenkins.server]/me/configure`)

2. Move the `server_config.json` file to the `/etc/indicator`

3. Make the `server_config.json` file readable only by a superuser (since it contains Jenkins access credentials!)

4. Set your project `name - device ID` mapping in the `indicator_config.json` file

5. Copy the `indicator_config.json` file to the `/etc/indicator` directory (depending on your preferences, you may also restrict access to this file)

6. Set DIR in `indicator.service` file to your location of the repository

7. Copy the `indicator.service` file to `/etc/init.d` directory

8. Give superuser root Read Write and Execute permissions to the `indicator.service` file

9. After the given steps, you should be able to start and stop the service and see its status using following commands:

```bash
sudo service indicator start
sudo service indicator stop
sudo service indicator status
```

If you would like to make the service start at bootup you can use `update-rc.d` script to do that:
```bash
sudo update-rc.d indicator defaults
```

### Contribution guidelines ###

* Feel free to publish pull requests for the project, we're open for any suggestions from you!

### Who do I talk to? ###

* Please use Bitbucket Issues on the project page.